package de.florianreinhard.test;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class AmazonSampleTest {

    private WebDriver chromeDriver;

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        chromeDriver = new ChromeDriver();
    }

    @Test
    public void searchSeleniumBook() throws Exception {
        // 1. Go to www.amazon.de
        chromeDriver.get("http://www.amazon.de");

        // 2. Enter search keywords "selenium java"
        WebElement searchInput = chromeDriver.findElement(By.cssSelector("#twotabsearchtextbox"));
        searchInput.sendKeys("selenium java");

        // 3. Click the search button
        WebElement searchButton = chromeDriver.findElement(By.cssSelector(".nav-search-submit input"));
        searchButton.click();

        // 4. Fetch search result items
        List<WebElement> results =  chromeDriver.findElements(By.cssSelector("li[id^='result']"));

        // 5. Verify the existence of search results
        Assert.assertTrue(results.size() > 1);

        // 6. Click on the linked headline of the first element
        WebElement firstResultHeadline = results.get(0).findElement(By.cssSelector("h2"));
        firstResultHeadline.click();

        // 7. Verify that a product image can be seen
        chromeDriver.findElement(By.cssSelector("div[id$='img-canvas']"));
    }

    @After
    public void tearDown() throws Exception {
        chromeDriver.quit();
    }

}
